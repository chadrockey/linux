// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (C) 2021 BayLibre, SAS.
 * Author: Fabien Parent <fparent@baylibre.com>
 */
/dts-v1/;
#include "mt8195-evb.dts"
#include "mt6359.dtsi"
#include <dt-bindings/gpio/gpio.h>

/ {
	model = "MediaTek MT8195 evaluation board with UFS storage";
	compatible = "mediatek,mt8195-evb", "mediatek,mt8195";

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		/* 12 MiB reserved for OP-TEE (BL32)
		 * +-----------------------+ 0x43e0_0000
		 * |      SHMEM 2MiB       |
		 * +-----------------------+ 0x43c0_0000
		 * |        | TA_RAM  8MiB |
		 * + TZDRAM +--------------+ 0x4340_0000
		 * |        | TEE_RAM 2MiB |
		 * +-----------------------+ 0x4320_0000
		 */
		optee_reserved: optee@43200000 {
			no-map;
			reg = <0 0x43200000 0 0x00c00000>;
		};

		/* 192 KiB reserved for ARM Trusted Firmware (BL31) */
		bl31_secmon_reserved: secmon@54600000 {
			no-map;
			reg = <0 0x54600000 0x0 0x30000>;
		};
	};

	firmware {
		optee {
			compatible = "linaro,optee-tz";
			method = "smc";
		};
	};
};

&mmc0 {
	status = "disabled";
};

&ufshci {
	status = "okay";
	vcc-supply = <&mt6359_vemc_1_ldo_reg>;
	vccq2-supply = <&mt6359_vufs_ldo_reg>;
};

&ufsphy {
	status = "okay";
};

&pmic {
	interrupt-parent = <&pio>;
	interrupts = <222 IRQ_TYPE_LEVEL_HIGH>;
};

&mt6359_vgpu11_buck_reg {
	regulator-always-on;
};

&mt6359_vpu_buck_reg {
	regulator-always-on;
};

&mt6359_vcore_buck_reg {
	regulator-always-on;
};

&mt6359_vproc1_buck_reg {
	regulator-always-on;
};

&mt6359_vproc2_buck_reg {
	regulator-always-on;
};

&mt6359_vsram_md_ldo_reg {
	regulator-always-on;
};

&mt6359_vbbck_ldo_reg {
	regulator-always-on;
};

&mt6359_vrf12_ldo_reg {
	regulator-always-on;
};
