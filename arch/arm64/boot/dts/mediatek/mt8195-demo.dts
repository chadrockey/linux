// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (C) 2021 BayLibre, SAS.
 * Author: Fabien Parent <fparent@baylibre.com>
 */
/dts-v1/;
#include "mt8195.dtsi"
#include "mt6359.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/pinctrl/mt8195-pinfunc.h>
#include <dt-bindings/usb/pd.h>

/ {
	model = "MediaTek MT8195 demo board";
	compatible = "mediatek,mt8195-demo", "mediatek,mt8195";

	aliases {
		serial0 = &uart0;
	};

	chosen {
		stdout-path = "serial0:921600n8";
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 0 0x80000000>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		/* 12 MiB reserved for OP-TEE (BL32)
		 * +-----------------------+ 0x43e0_0000
		 * |      SHMEM 2MiB       |
		 * +-----------------------+ 0x43c0_0000
		 * |        | TA_RAM  8MiB |
		 * + TZDRAM +--------------+ 0x4340_0000
		 * |        | TEE_RAM 2MiB |
		 * +-----------------------+ 0x4320_0000
		 */
		optee_reserved: optee@43200000 {
			no-map;
			reg = <0 0x43200000 0 0x00c00000>;
		};

		/* 192 KiB reserved for ARM Trusted Firmware (BL31) */
		bl31_secmon_reserved: secmon@54600000 {
			no-map;
			reg = <0 0x54600000 0x0 0x30000>;
		};
	};

	firmware {
		optee {
			compatible = "linaro,optee-tz";
			method = "smc";
		};
	};

	gpio-keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";

		volume-up {
			wakeup-source;
			debounce-interval = <100>;
			gpios = <&pio 106 GPIO_ACTIVE_LOW>;
			label = "volume_up";
			linux,code = <KEY_VOLUMEUP>;
		};
	};

	mtk_fsource: fsource {
		compatible = "mtk-fsource";
		vfsource-supply = <&mt6359_vefuse_ldo_reg>;
	};
};

&uart0 {
	status = "okay";
};

&mmc0 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc0_pins_default>;
	pinctrl-1 = <&mmc0_pins_uhs>;
	bus-width = <8>;
	max-frequency = <200000000>;
	cap-mmc-highspeed;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	cap-mmc-hw-reset;
	no-sdio;
	no-sd;
	hs400-ds-delay = <0x14c11>;
	vmmc-supply = <&mt6359_vemc_1_ldo_reg>;
	vqmmc-supply = <&mt6359_vufs_ldo_reg>;
	non-removable;
};

&pmic {
	interrupt-parent = <&pio>;
	interrupts = <222 IRQ_TYPE_LEVEL_HIGH>;
};

&i2c0 {
	clock-frequency = <400000>;
	pinctrl-0 = <&i2c0_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-0 = <&i2c1_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&i2c2 {
	clock-frequency = <400000>;
	pinctrl-0 = <&i2c2_pins>;
	pinctrl-names = "default";
	status = "okay";

	it5205fn: it5205fn@48 {
		compatible = "mediatek,it5205fn";
		reg = <0x48>;
		type3v3-supply = <&mt6359_vibr_ldo_reg>;
		status = "okay";
	};
};

&i2c6 {
	clock-frequency = <400000>;
	pinctrl-0 = <&i2c6_pins>;
	pinctrl-names = "default";
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	mt6360: mt6360@34 {
		compatible = "mediatek,mt6360";
		reg = <0x34>;
		pinctrl-0 = <&mt6360_pins>;
		pinctrl-names = "default";

		tcpc {
			compatible = "mediatek,mt6360-tcpc";
			interrupts-extended = <&pio 100 IRQ_TYPE_LEVEL_LOW>;
			interrupt-names = "PD_IRQB";

			connector {
				compatible = "usb-c-connector";
				label = "USB-C";
				data-role = "dual";
				power-role = "dual";
				try-power-role = "sink";
				source-pdos = <PDO_FIXED(5000, 1000, PDO_FIXED_DUAL_ROLE | PDO_FIXED_DATA_SWAP)>;
				sink-pdos = <PDO_FIXED(5000, 2000, PDO_FIXED_DUAL_ROLE | PDO_FIXED_DATA_SWAP)>;
				op-sink-microwatt = <10000000>;
			};

			ports {
				#address-cells = <1>;
				#size-cells = <0>;

				port@0 {
					reg = <0>;
					mt6360_ssusb_ep: endpoint {
						remote-endpoint = <&ssusb_ep>;
					};
				};

			};
		};
	};
};

&spi2 {
	pinctrl-0 = <&spi2_pins>;
	pinctrl-names = "default";
	mediatek,pad-select = <0>;
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	spidev@0 {
		compatible = "mediatek,aiot-board";
		spi-max-frequency = <5000000>;
		reg = <0>;
	};
};

&ssusb {
	pinctrl-names = "default";
	pinctrl-0 = <&u3_p0_vbus>;
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
	dr_mode = "otg";
	mediatek,usb3-drd;
	usb-role-switch;
	status = "okay";

	port {
		ssusb_ep: endpoint {
			remote-endpoint = <&mt6360_ssusb_ep>;
		};
	};
};

&xhci3 {
	status = "okay";
};

&xhci0 {
	vbus-supply = <&otg_vbus_regulator>;
	status = "okay";
};

&xhci1 {
	status = "okay";
};

&u3port0 {
	status = "okay";
};

&u2port1 {
	status = "okay";
};

&u2port3 {
	status = "okay";
};

&u3phy0 {
	status="okay";
};

&u3phy3 {
	status="okay";
};

&eth {
	phy-mode ="rgmii-rxid";
	phy-handle = <&eth_phy0>;
	snps,reset-gpio = <&pio 93 GPIO_ACTIVE_HIGH>;
	snps,reset-delays-us = <0 10000 10000>;
	mediatek,tx-delay-ps = <2030>;
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&eth_default>;
	pinctrl-1 = <&eth_sleep>;
	status = "okay";

	mdio {
		compatible = "snps,dwmac-mdio";
		#address-cells = <1>;
		#size-cells = <0>;
		eth_phy0: eth_phy0@1 {
			compatible = "ethernet-phy-id001c.c916";
			reg = <0x1>;
		};
	};
};

&pio {
	u3_p0_vbus: u3_p0vbusdefault {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO63__FUNC_VBUSVALID>;
			input-enable;
		};
	};

	mmc0_pins_default: mmc0default {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO126__FUNC_MSDC0_DAT0>,
				 <PINMUX_GPIO125__FUNC_MSDC0_DAT1>,
				 <PINMUX_GPIO124__FUNC_MSDC0_DAT2>,
				 <PINMUX_GPIO123__FUNC_MSDC0_DAT3>,
				 <PINMUX_GPIO119__FUNC_MSDC0_DAT4>,
				 <PINMUX_GPIO118__FUNC_MSDC0_DAT5>,
				 <PINMUX_GPIO117__FUNC_MSDC0_DAT6>,
				 <PINMUX_GPIO116__FUNC_MSDC0_DAT7>,
				 <PINMUX_GPIO121__FUNC_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO122__FUNC_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins_rst {
			pinmux = <PINMUX_GPIO120__FUNC_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc0_pins_uhs: mmc0uhs{
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO126__FUNC_MSDC0_DAT0>,
				 <PINMUX_GPIO125__FUNC_MSDC0_DAT1>,
				 <PINMUX_GPIO124__FUNC_MSDC0_DAT2>,
				 <PINMUX_GPIO123__FUNC_MSDC0_DAT3>,
				 <PINMUX_GPIO119__FUNC_MSDC0_DAT4>,
				 <PINMUX_GPIO118__FUNC_MSDC0_DAT5>,
				 <PINMUX_GPIO117__FUNC_MSDC0_DAT6>,
				 <PINMUX_GPIO116__FUNC_MSDC0_DAT7>,
				 <PINMUX_GPIO121__FUNC_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO122__FUNC_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins_ds {
			pinmux = <PINMUX_GPIO127__FUNC_MSDC0_DSL>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins_rst {
			pinmux = <PINMUX_GPIO120__FUNC_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc1_pins_default: mmc1-pins-default {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO110__FUNC_MSDC1_CMD>,
				 <PINMUX_GPIO112__FUNC_MSDC1_DAT0>,
				 <PINMUX_GPIO113__FUNC_MSDC1_DAT1>,
				 <PINMUX_GPIO114__FUNC_MSDC1_DAT2>,
				 <PINMUX_GPIO115__FUNC_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO111__FUNC_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins_insert {
			pinmux = <PINMUX_GPIO129__FUNC_GPIO129>;
			bias-pull-up;
		};
	};

	mmc1_pins_uhs: mmc1-pins-uhs {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO110__FUNC_MSDC1_CMD>,
				 <PINMUX_GPIO112__FUNC_MSDC1_DAT0>,
				 <PINMUX_GPIO113__FUNC_MSDC1_DAT1>,
				 <PINMUX_GPIO114__FUNC_MSDC1_DAT2>,
				 <PINMUX_GPIO115__FUNC_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO111__FUNC_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
	};

	i2c0_pins: i2c0-pins {
		pins {
			pinmux = <PINMUX_GPIO8__FUNC_SDA0>,
				 <PINMUX_GPIO9__FUNC_SCL0>;
			bias-pull-up;
			mediatek,rsel = <MTK_PULL_SET_RSEL_111>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c1_pins: i2c1-pins {
		pins {
			pinmux = <PINMUX_GPIO10__FUNC_SDA1>,
				 <PINMUX_GPIO11__FUNC_SCL1>;
			bias-pull-up;
			mediatek,rsel = <MTK_PULL_SET_RSEL_111>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c2_pins: i2c2-pins {
		pins {
			pinmux = <PINMUX_GPIO12__FUNC_SDA2>,
				 <PINMUX_GPIO13__FUNC_SCL2>;
			bias-pull-up;
			mediatek,rsel = <MTK_PULL_SET_RSEL_111>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c6_pins: i2c6-pin {
		pins {
			pinmux = <PINMUX_GPIO25__FUNC_SDA6>,
				 <PINMUX_GPIO26__FUNC_SCL6>;
			bias-pull-up;
			mediatek,rsel = <MTK_PULL_SET_RSEL_111>;
		};
	};

	spi2_pins: spi-pins {
		pins {
			pinmux = <PINMUX_GPIO140__FUNC_SPIM2_CSB>,
				 <PINMUX_GPIO141__FUNC_SPIM2_CLK>,
				 <PINMUX_GPIO142__FUNC_SPIM2_MO>,
				 <PINMUX_GPIO143__FUNC_SPIM2_MI>;
			bias-disable;
		};
	};

	eth_default: eth_default {
		txd_pins {
			pinmux = <PINMUX_GPIO77__FUNC_GBE_TXD3>,
				 <PINMUX_GPIO78__FUNC_GBE_TXD2>,
				 <PINMUX_GPIO79__FUNC_GBE_TXD1>,
				 <PINMUX_GPIO80__FUNC_GBE_TXD0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		cc_pins {
			pinmux = <PINMUX_GPIO85__FUNC_GBE_TXC>,
				 <PINMUX_GPIO88__FUNC_GBE_TXEN>,
				 <PINMUX_GPIO87__FUNC_GBE_RXDV>,
				 <PINMUX_GPIO86__FUNC_GBE_RXC>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		rxd_pins {
			pinmux = <PINMUX_GPIO81__FUNC_GBE_RXD3>,
				 <PINMUX_GPIO82__FUNC_GBE_RXD2>,
				 <PINMUX_GPIO83__FUNC_GBE_RXD1>,
				 <PINMUX_GPIO84__FUNC_GBE_RXD0>;
		};
		mdio_pins {
			pinmux = <PINMUX_GPIO89__FUNC_GBE_MDC>,
				 <PINMUX_GPIO90__FUNC_GBE_MDIO>;
			input-enable;
		};
		power_pins {
			pinmux = <PINMUX_GPIO91__FUNC_GPIO91>,
				 <PINMUX_GPIO92__FUNC_GPIO92>;
			output-high;
		};
		phy_reset_pin {
			pinmux = <PINMUX_GPIO93__FUNC_GPIO93>;
		};
	};

	eth_sleep: eth_sleep {
		txd_pins {
			pinmux = <PINMUX_GPIO77__FUNC_GPIO77>,
				 <PINMUX_GPIO78__FUNC_GPIO78>,
				 <PINMUX_GPIO79__FUNC_GPIO79>,
				 <PINMUX_GPIO80__FUNC_GPIO80>;
		};
		cc_pins {
			pinmux = <PINMUX_GPIO85__FUNC_GPIO85>,
				 <PINMUX_GPIO88__FUNC_GPIO88>,
				 <PINMUX_GPIO87__FUNC_GPIO87>,
				 <PINMUX_GPIO86__FUNC_GPIO86>;
		};
		rxd_pins {
			pinmux = <PINMUX_GPIO81__FUNC_GPIO81>,
				 <PINMUX_GPIO82__FUNC_GPIO82>,
				 <PINMUX_GPIO83__FUNC_GPIO83>,
				 <PINMUX_GPIO84__FUNC_GPIO84>;
		};
		mdio_pins {
			pinmux = <PINMUX_GPIO89__FUNC_GPIO89>,
				 <PINMUX_GPIO90__FUNC_GPIO90>;
			input-disable;
			bias-disable;
		};
		power_pins {
			pinmux = <PINMUX_GPIO91__FUNC_GPIO91>,
				 <PINMUX_GPIO92__FUNC_GPIO92>;
			input-disable;
			bias-disable;
		};
		phy_reset_pin {
			pinmux = <PINMUX_GPIO93__FUNC_GPIO93>;
			input-disable;
			bias-disable;
		};
	};

	pcie0_pins_default: pcie0default {
		pins {
			pinmux = <PINMUX_GPIO19__FUNC_WAKEN>,
				 <PINMUX_GPIO20__FUNC_PERSTN>,
				 <PINMUX_GPIO21__FUNC_CLKREQN>;
			bias-pull-up;
		};
	};

	pcie1_pins_default: pcie1default {
		pins {
			pinmux = <PINMUX_GPIO22__FUNC_PERSTN_1>,
				 <PINMUX_GPIO23__FUNC_CLKREQN_1>,
				 <PINMUX_GPIO24__FUNC_WAKEN_1>;
			bias-pull-up;
		};

		mt7921 {
			pinmux = <PINMUX_GPIO65__FUNC_GPIO65>,
				 <PINMUX_GPIO67__FUNC_GPIO67>;
			output-high;
		};
	};

	gpio_keys: gpio-keys {
		pins {
			pinmux = <PINMUX_GPIO106__FUNC_GPIO106>;
			bias-pull-up;
			input-enable;
		};
	};

	mt6360_pins: mt6360-pins {
		pins {
			pinmux = <PINMUX_GPIO100__FUNC_GPIO100>,
				 <PINMUX_GPIO101__FUNC_GPIO101>;
			input-enable;
			bias-pull-up;
		};
	};
};

&mt6359_vgpu11_buck_reg {
	regulator-always-on;
};

&mt6359_vsram_others_ldo_reg {
	regulator-always-on;
};

&mt6359_vpu_buck_reg {
	regulator-always-on;
};

&mt6359_vcore_buck_reg {
	regulator-always-on;
};

&mt6359_vproc1_buck_reg {
	regulator-always-on;
};

&mt6359_vproc2_buck_reg {
	regulator-always-on;
};

&mt6359_vsram_md_ldo_reg {
	regulator-always-on;
};

&mt6359_vbbck_ldo_reg {
	regulator-always-on;
};

&mt6359_vaud18_ldo_reg {
	regulator-always-on;
};

&mt6359_vrf12_ldo_reg {
	regulator-always-on;
};

/* DEBUG: to remove */
&mt6359_vibr_ldo_reg {
	regulator-always-on;
};

#include "mt6360.dtsi"

/* For EMI_VDD2 */
&mt6360_buck1 {
	regulator-always-on;
};

/* For EMI_VDDQ */
&mt6360_buck2 {
	regulator-always-on;
};

/* For EMI_VMDDR_EN */
&mt6360_ldo7 {
	regulator-always-on;
};

&mmc1 {
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc1_pins_default>;
	pinctrl-1 = <&mmc1_pins_uhs>;
	cd-gpios = <&pio 129 GPIO_ACTIVE_LOW>;
	bus-width = <4>;
	max-frequency = <200000000>;
	cap-sd-highspeed;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	vmmc-supply = <&mt6360_ldo5>;
	vqmmc-supply = <&mt6360_ldo3>;
	status = "okay";
};

&spmi {
	#address-cells = <2>;
	#size-cells = <2>;
	grpid = <11>;

	mt6315_6: mt6315@6 {
		compatible = "mediatek,mt6315-regulator";
		reg = <0x6 0 0xb 1>;

		regulators {
			mt6315_6_vbuck1: vbuck1 {
				regulator-compatible = "vbuck1";
				regulator-name = "Vbcpu";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2 4>;
				regulator-always-on;
			};
		};
	};

	mt6315_7: mt6315@7 {
		compatible = "mediatek,mt6315-regulator";
		reg = <0x7 0 0xb 1>;

		regulators {
			mt6315_7_vbuck1: vbuck1 {
				regulator-compatible = "vbuck1";
				regulator-name = "Vgpu";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2 4>;
				regulator-always-on;
			};
		};
	};
};

&mfg0 {
	domain-supply = <&mt6315_7_vbuck1>;
};

&pcie0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pcie0_pins_default>;
	status = "okay";
};

&pcie1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pcie1_pins_default>;
	status = "okay";
};

&u3phy1 {
	status = "okay";
};

&pciephy {
	status = "okay";
};
